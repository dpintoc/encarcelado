class Encarcelado {
    //Atributos
    _palabra;
    _letra;

    constructor(valor) {
        this._palabra = valor;
    }

    set palabra(valor) {
        this._palabra = valor;
    }

    set letra(valor) {
        this._letra = valor;
    }

    iniciarJuego() {
            // Verificar el tamaño de la palabra.

            let a = this._palabra.length;
            let i;
            let formulario;
            let boton;

            formulario = document.getElementById("tablero");

            for (i = 0; i < a; i++) {
                //Crear botones
                boton = document.createElement("input");
                boton.setAttribute("type", "button");
                boton.setAttribute("class", "boton");
                boton.setAttribute("id", "boton" + i);

                formulario.appendChild(boton);
            }
        }
        //Verificar si la letra pertenece a la palabra
    verificarLetra() {
        let b = this._palabra.length;
        let u = ["u", "n", "i", "v", "e", "r", "s", "i", "d", "a", "d"];
        let i = 0;
        while (i < b) {
            if (this._letra == u[i]) {
                alert("La letra se encuentra en la palabra.");
                return this._letra;
            } else {
                alert("La letra no se encuentra en la palabra.");
                return this._letra;
            }
        }
    }
}

let miJuego = new Encarcelado("universidad");
miJuego.iniciarJuego();